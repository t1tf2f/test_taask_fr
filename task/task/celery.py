import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'task.settings')


app = Celery('task')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.task(bind=True, ignore_result=True)
def debug_task(self):
    print(f'Request: {self.request!r}')

app.conf.beat_schedule = {
    'run-me-every-minute': {
        'task': 'index.tasks.send_beat_mailing',
        'schedule': crontab(minute='*/1'),
    }
}
