from task.celery import app
from .logger import Logger
from .models import *
import time
import requests
import json

@app.task
def send_beat_mailing():
    logger = Logger().getLogger("DEBUG")
    logger.info('test log in beat')
    timestamp_now = time.time()
    mailings = Mailing.objects.all()
    for mailing in mailings:
        date_start = str(mailing.date_start)[:-6]
        date_end = str(mailing.date_end)[:-6]
        timestamp_start_mailing = int(time.mktime(time.strptime(date_start, '%Y-%m-%d %H:%M:%S'))) - time.timezone
        timestamp_end_mailing = int(time.mktime(time.strptime(date_end, '%Y-%m-%d %H:%M:%S'))) - time.timezone
        if timestamp_start_mailing < timestamp_now < timestamp_end_mailing:
            clients = Client.objects.filter(tag=mailing.tag)
            for client in clients:
                messages = Message.objects.filter(client=client)
                for message in messages:
                    if message.status == 'success':
                        continue
                    data_to_send = {
                        'id': int(message.id),
                        'phone': int(client.phone_number),
                        'text': mailing.msg
                    }
                    with open('token.json', 'r') as fp:
                        token = json.loads(fp)
                    r = send_request_mailing(int(message.id), token['token'], data_to_send)
                    if r.status_code == 200:
                        logger.info(
                            'Сообщение удачно отправлено.\nId сообщения = {},\nТекст сообщения = {},\nId рассылки = {},\nId пользователя = {},\nКод ответа = {},\n Текст ответа запроса = {}'.format(
                                message.id,
                                mailing.msg,
                                mailing.id,
                                client.id,
                                r.data['code'],
                                r.data['message']
                            )
                        )
                        message.status = 'success'
                        message.save()
                    else:
                        logger.info(
                            'Сообщение не отправлено.\nId сообщения = {},\nТекст сообщения = {},\nId рассылки = {},\nId пользователя = {},\nКод ответа = {},\n Текст ответа запроса = {}'.format(
                                message.id,
                                mailing.msg,
                                mailing.id,
                                client.id,
                            )
                        )
                        message.status = 'fail'
                        message.save()
    return 'ok'


def send_request_mailing(msg_id, token, data_to_send):
    resp = requests.post(
        'https://probe.fbrq.cloud/v1/send/{}'.format(msg_id),
        headers={
            'Authorization': token,
            'Content-Type': 'application/json',
        },
        params={'msgId': msg_id},
        json=data_to_send
    )
    return resp
