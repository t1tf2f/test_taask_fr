import logging
import os

class Logger:
	formatter = logging.Formatter('%(asctime)s %(levelname)s:%(message)s', datefmt='%d.%m.%y %H:%M:%S')
	INFO = logging.INFO
	DEBUG = logging.DEBUG
	ERROR = logging.ERROR
	WARNING = logging.WARNING

	def __init__(self):
		self.logger = None
		self.handler = None
		self.log_path = os.path.join(os.path.dirname(__file__), "log/")
		self.log_file = self.log_path + "log.log"
		if not os.path.exists(self.log_path):
			os.makedirs(self.log_path)
			with open(self.log_file, 'w+'):
				pass

	def getLogger(self, level='INFO'):
		if level == 'INFO':
			level = logging.INFO
		elif level == 'DEBUG':
			level = logging.DEBUG
		elif level == 'ERROR':
			level = logging.ERROR
		elif level == 'WARNING':
			level = logging.WARNING
		self.handler = logging.FileHandler(self.log_file)
		self.handler.setFormatter(Logger.formatter)
		self.logger = logging.getLogger("logger")
		self.logger.setLevel(level)
		self.logger.addHandler(self.handler)
		return self.logger
