from rest_framework import serializers
from .models import *
import re
from .logger import Logger
logger = Logger().getLogger("DEBUG")

class ClientCreateSerializer(serializers.Serializer):
    phone_number = serializers.CharField(max_length=12)
    code = serializers.CharField(max_length=5)
    tag = serializers.CharField(max_length=63)
    time_zone = serializers.IntegerField()

    def create(self, validated_data):
        return Client.objects.create(**validated_data)

    def validate(self, data):
        pattern = re.compile(r'^7\d{10}')
        match = pattern.match(data['phone_number'])
        if not match:
            logger.info('Ошибка при создании клиента, не корректный номер')
            raise serializers.ValidationError({"status": "fail", "desc": "Не корректный формат номера."})
        return data


class ClientUpdateSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    phone_number = serializers.CharField(max_length=12, required=False)
    code = serializers.CharField(max_length=5, required=False)
    tag = serializers.CharField(max_length=63, required=False)
    time_zone = serializers.IntegerField(required=False)

    def validate(self, data):
        if 'phone_number' in data:
            pattern = re.compile(r'^7\d{10}')
            match = pattern.match(data['phone_number'])
            if not match:
                logger.info('Ошибка при обновлении клиента, не корректный номер')
                raise serializers.ValidationError({"status": "fail", "desc": "Не корректный формат номера."})
            return data
        return data

    def create(self, validated_data):
        try:
            client = Client.objects.get(id=validated_data['id'])
        except:
            logger.info('Ошибка при обновлении клиента, клиент с id = {} не найден'.format(validated_data['id']))
            raise serializers.ValidationError({"status": "fail", "desc": "Объект клиента не существует."})
        if 'phone_number' in validated_data:
            client.phone_number = validated_data['phone_number']
        if 'code' in validated_data:
            client.code = validated_data['code']
        if 'tag' in validated_data:
            client.tag = validated_data['tag']
        if 'time_zone' in validated_data:
            client.time_zone = validated_data['time_zone']
        client.save()
        return client


class ClientDeleteSerializer(serializers.Serializer):
    id = serializers.IntegerField()

    def create(self, validated_data):
        try:
            client = Client.objects.get(id=validated_data['id'])
        except:
            logger.info('Ошибка при удалении клиента, клиент с id = {} не найден'.format(validated_data['id']))
            raise serializers.ValidationError({"status": "fail", "desc": "Объект клиента не существует."})
        return client


class MailingCreateSerializer(serializers.Serializer):
    date_start = serializers.DateTimeField(input_formats=['%Y-%m-%dT%H:%M:%S'])
    date_end = serializers.DateTimeField(input_formats=['%Y-%m-%dT%H:%M:%S'])
    msg = serializers.CharField(max_length=255)
    tag = serializers.CharField(max_length=63)

    def create(self, validate_data):
        return Mailing.objects.create(**validate_data)


class MailingUpdateSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    date_start = serializers.DateTimeField(input_formats=['%Y-%m-%dT%H:%M:%S'], required=False)
    date_end = serializers.DateTimeField(input_formats=['%Y-%m-%dT%H:%M:%S'], required=False)
    msg = serializers.CharField(max_length=255, required=False)
    tag = serializers.CharField(max_length=63, required=False)

    def create(self, validated_data):
        try:
            mailing = Mailing.objects.get(id=validated_data['id'])
        except:
            logger.info('Ошибка при обновлении рассылки, рассылка с id = {} не найдена'.format(validated_data['id']))
            raise serializers.ValidationError({"status": "fail", "desc": "Объект рассылки не существует."})
        if "data_start" in validated_data:
            mailing.date_start = validated_data['data_start']
        if "data_end" in validated_data:
            mailing.date_end = validated_data['data_end']
        if "msg" in validated_data:
            mailing.msg = validated_data['msg']
        if "tag" in validated_data:
            mailing.tag = validated_data['tag']
        mailing.save()
        return mailing


class MailingDeleteSerializer(serializers.Serializer):
    id = serializers.IntegerField()

    def create(self, validated_data):
        try:
            mailing = Mailing.objects.get(id=validated_data['id'])
        except:
            logger.info('Ошибка при удалении рассылки, рассылка с id = {} не найдена'.format(validated_data['id']))
            raise serializers.ValidationError({"status": "fail", "desc": "Объект рассылки не существует."})
        return mailing


class GetDetailStatisticSerializer(serializers.Serializer):
    id = serializers.IntegerField()

    def create(self, validated_data):
        try:
            return Mailing.objects.get(id=validated_data['id'])
        except:
            logger.info('Ошибка при получения полной статистики рассылки, рассылка с id = {} не найдена'.format(validated_data['id']))
            raise serializers.ValidationError({"status": "fail", "desc": "Объект рассылки не существует."})


# Сериализаторы для респонсов в сваггере
class RespGetGlobalStaticSerializer(serializers.Serializer):
    mailing = serializers.ListSerializer(child=serializers.DictField())


class RespStatusOK(serializers.Serializer):
    status = serializers.CharField(default='ok')


class RespStatusOK_Id(serializers.Serializer):
    id = serializers.IntegerField()
    status = serializers.CharField(default='ok')


class RespStatusFail(serializers.Serializer):
    status = serializers.CharField(default='fail')
    desc = serializers.CharField()

class RespStatusOKDetailStatistic(serializers.Serializer):
    status = serializers.CharField(default='success')
    id = serializers.IntegerField()
    messages = serializers.ListSerializer(child=serializers.DictField())

