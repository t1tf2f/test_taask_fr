from django.contrib import admin
from .models import *


@admin.register(Client)
class ClientModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Mailing)
class MailingModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Message)
class MessageModelAdmin(admin.ModelAdmin):
    pass
