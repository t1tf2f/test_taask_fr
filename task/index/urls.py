from django.urls import path
from .views import *

urlpatterns = [
    path('create_client/', CreateClient.as_view()),
    path('update_client/', UpdateClient.as_view()),
    path('delete_client/', DeleteClient.as_view()),
    path('create_mailing/', CreateMailing.as_view()),
    path('update_mailing/', UpdateMailing.as_view()),
    path('delete_mailing/', DeletingMailing.as_view()),
    path('get_global_statistic/', GetGlobalStatistic.as_view()),
    path('get_detail_statistic/', GetDetailStatistic.as_view()),
    path('', IndexPage)
]
