window.addEventListener('load', function(e) {
    let bars = document.getElementsByClassName('bar')
    bars[0].addEventListener('click', function(e) {
        e.preventDefault();
        hideAllMenu();
        document.getElementById('menu-1').classList.remove('hiden');
        e.target.classList.remove('unselected');
    });

    bars[1].addEventListener('click', function(e) {
        e.preventDefault();
        hideAllMenu();
        document.getElementById('menu-2').classList.remove('hiden');
        e.target.classList.remove('unselected');
    });

    bars[2].addEventListener('click', function(e) {
        e.preventDefault();
        hideAllMenu();
        document.getElementById('menu-3').classList.remove('hiden');
        e.target.classList.remove('unselected');
    });

    let menu3 = document.getElementById('menu-3')
    menu3.getElementsByClassName('left_container')[0]
         .getElementsByClassName('button_container')[0]
         .getElementsByTagName('a')[0]
         .addEventListener('click', function(e) {
            e.preventDefault();
            requestGetGlobalStatistic();
        });

    menu3.getElementsByClassName('left_container')[0]
         .getElementsByClassName('button_input_container')[0]
         .getElementsByTagName('a')[0]
         .addEventListener('click', function(e) {
            e.preventDefault();
                requestGetDetailStatistic();
            });
    
    document.getElementById('create-mailing').addEventListener('click', function(e) {
        e.preventDefault();
        let menu1 = document.getElementById('menu-1');
        menu1.getElementsByClassName('right_container')[0].classList.remove('hiden');
        menu1.getElementsByClassName('type_action_container')[0].innerHTML = 'Создание рассылки';

        menu1.getElementsByClassName('status_request')[0].innerHTML = 'Статус запроса:';
        
        menu1.getElementsByClassName('right_container')[0]
             .getElementsByClassName('button_container')[0]
             .getElementsByTagName('a')[0]
             .setAttribute('type', 'create');
        
        menu1.getElementsByClassName('inputs_container')[0].innerHTML = 
                                                                    '<div class="input_container">      \
                                                                        <div>date_start</div>           \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>date_end</div>             \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>msg</div>                  \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>tag</div>                  \
                                                                        <input>                         \
                                                                    </div>'
    });

    document.getElementById('delete-mailing').addEventListener('click', function(e) {
        e.preventDefault();
        let menu1 = document.getElementById('menu-1');
        menu1.getElementsByClassName('right_container')[0].classList.remove('hiden');
        menu1.getElementsByClassName('type_action_container')[0].innerHTML = 'Удаление рассылки';

        menu1.getElementsByClassName('status_request')[0].innerHTML = 'Статус запроса:';

        menu1.getElementsByClassName('right_container')[0]
             .getElementsByClassName('button_container')[0]
             .getElementsByTagName('a')[0]
             .setAttribute('type', 'delete');
        
        menu1.getElementsByClassName('inputs_container')[0].innerHTML = 
                                                                    '<div class="input_container">      \
                                                                        <div>id</div>                   \
                                                                        <input "number" step="1">       \
                                                                    </div>'
    });

    document.getElementById('edit-mailing').addEventListener('click', function(e) {
        e.preventDefault();
        let menu1 = document.getElementById('menu-1');
        menu1.getElementsByClassName('right_container')[0].classList.remove('hiden');
        menu1.getElementsByClassName('type_action_container')[0].innerHTML = 'Редактирование рассылки';

        menu1.getElementsByClassName('status_request')[0].innerHTML = 'Статус запроса:';

        menu1.getElementsByClassName('right_container')[0]
             .getElementsByClassName('button_container')[0]
             .getElementsByTagName('a')[0]
             .setAttribute('type', 'edit');
        
        menu1.getElementsByClassName('inputs_container')[0].innerHTML = 
                                                                    '<div class="input_container">      \
                                                                        <div>id</div>                   \
                                                                        <input "number" step="1">       \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>date_start</div>           \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>date_end</div>             \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>msg</div>                  \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>tag</div>                  \
                                                                        <input>                         \
                                                                    </div>'
    });

    document.getElementById('create-client').addEventListener('click', function(e) {
        e.preventDefault();
        let menu2 = document.getElementById('menu-2');
        menu2.getElementsByClassName('right_container')[0].classList.remove('hiden');
        menu2.getElementsByClassName('type_action_container')[0].innerHTML = 'Создание пользователя';

        menu2.getElementsByClassName('status_request')[0].innerHTML = 'Статус запроса:';

        menu2.getElementsByClassName('right_container')[0]
             .getElementsByClassName('button_container')[0]
             .getElementsByTagName('a')[0]
             .setAttribute('type', 'create');
        
        menu2.getElementsByClassName('inputs_container')[0].innerHTML = 
                                                                    '<div class="input_container">      \
                                                                        <div>phone_number</div>         \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>code</div>                 \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>tag</div>                  \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>time_zone</div>            \
                                                                        <input>                         \
                                                                    </div>'
    });

    document.getElementById('delete-client').addEventListener('click', function(e) {
        e.preventDefault();
        let menu2 = document.getElementById('menu-2');
        menu2.getElementsByClassName('right_container')[0].classList.remove('hiden');
        menu2.getElementsByClassName('type_action_container')[0].innerHTML = 'Удаление пользователя';

        menu2.getElementsByClassName('status_request')[0].innerHTML = 'Статус запроса:';

        menu2.getElementsByClassName('right_container')[0]
             .getElementsByClassName('button_container')[0]
             .getElementsByTagName('a')[0]
             .setAttribute('type', 'delete');
        
        menu2.getElementsByClassName('inputs_container')[0].innerHTML = 
                                                                    '<div class="input_container">      \
                                                                        <div>id</div>                   \
                                                                        <input "number" step="1">       \
                                                                    </div>'
    });

    document.getElementById('edit-client').addEventListener('click', function(e) {
        e.preventDefault();
        let menu2 = document.getElementById('menu-2');
        menu2.getElementsByClassName('right_container')[0].classList.remove('hiden');
        menu2.getElementsByClassName('type_action_container')[0].innerHTML = 'Редактирование пользователя';

        menu2.getElementsByClassName('status_request')[0].innerHTML = 'Статус запроса:';

        menu2.getElementsByClassName('right_container')[0]
             .getElementsByClassName('button_container')[0]
             .getElementsByTagName('a')[0]
             .setAttribute('type', 'edit');
        
        menu2.getElementsByClassName('inputs_container')[0].innerHTML = 
                                                                    '<div class="input_container">      \
                                                                        <div>id</div>                   \
                                                                        <input "number" step="1">       \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>phone_number</div>         \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>code</div>                 \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>tag</div>                  \
                                                                        <input>                         \
                                                                    </div>                              \
                                                                    <div class="input_container">       \
                                                                        <div>time_zone</div>            \
                                                                        <input>                         \
                                                                    </div>'
    });

    document.getElementById('menu-1')
            .getElementsByClassName('right_container')[0]
            .getElementsByClassName('button_container')[0]
            .getElementsByTagName('a')[0]
            .addEventListener('click', function(e) {
                e.preventDefault();
                let obj = e.target;
                let type = obj.getAttribute('type');
                let inputs = obj.parentElement.parentElement.getElementsByTagName('input');
                let inputs_value = [];
                for (let i = 0; i < inputs.length; i++) {
                    inputs_value.push(inputs[i].value);
                }
                sendRequestMenu1(type, inputs_value);
            });

    document.getElementById('menu-2')
            .getElementsByClassName('right_container')[0]
            .getElementsByClassName('button_container')[0]
            .getElementsByTagName('a')[0]
            .addEventListener('click', function(e) {
                e.preventDefault();
                let obj = e.target;
                let type = obj.getAttribute('type');
                let inputs = obj.parentElement.parentElement.getElementsByTagName('input');
                let inputs_value = [];
                for (let i = 0; i < inputs.length; i++) {
                    inputs_value.push(inputs[i].value);
                }
                sendRequestMenu2(type, inputs_value);
            });
});

function hideAllMenu() {
    let menu = document.getElementsByClassName('menu_container')
    for (let i = 0; i < menu.length; i++) {
        menu[i].classList.add('hiden');   
    }
    let top_bar_buttons = document.getElementsByClassName('top_bar_conatiner')[0]
                                    .getElementsByClassName('bar');
    for (let i = 0; i < top_bar_buttons.length; i++) {
        top_bar_buttons[i].classList.add('unselected');
    }
}

async function requestGetGlobalStatistic() {
    let response = await fetch('/get_global_statistic/', {
        method: 'GET'
    });
    var json_response = await response.json();
    let mailings = json_response['mailings'];
    let table = compileTable(mailings);
    document.getElementById('menu-3').getElementsByClassName('table_container')[0].innerHTML = table;
}

async function requestGetDetailStatistic() {
    let id = document.getElementById('menu-3')
                     .getElementsByClassName('left_container')[0]
                     .getElementsByClassName('button_input_container')[0]
                     .getElementsByTagName('input')[0]
                     .value;
    if (id == '' || id == undefined) {
        return
    }
    let json = {'id': id}
    let response = await fetch('/get_detail_statistic/', {
        method: 'POST',
        body: JSON.stringify(json),
        headers:{
			'Content-Type': 'application/json;charset=utf-8',
            'X-CSRFToken': document.cookie.split('csrftoken=')[1].split(';')[0]
        }
    })
    var json_response = await response.json();
    if (json_response['status'] == 'ok') {
        let messages = json_response['messages'];
        let table = compileDetailTable(messages);
        document.getElementById('menu-3').getElementsByClassName('table_container')[0].innerHTML = table;
    } else {
        if (json_response['desc'] == undefined) {
            document.getElementById('menu-3').getElementsByClassName('table_container')[0].innerHTML = 'Не корректный формат данных';
        } else {
            document.getElementById('menu-3').getElementsByClassName('table_container')[0].innerHTML = json_response['desc'];
        }
        
    }
}

function compileTable(mailings) {
    let table =
    '<div class="row first_row">        \
        <div class="cell cell_id">      \
            id                          \
        </div>                          \
        <div class="cell cell_msg">     \
            msg                         \
        </div>                          \
        <div class="cell cell_tag">     \
            tag                         \
        </div>                          \
        <div class="cell cell_date">    \
            date_start                  \
        </div>                          \
        <div class="cell cell_date">    \
            date_end                    \
        </div>                          \
        <div class="cell cell_status">  \
            success                     \
        </div>                          \
        <div class="cell cell_status">  \
            fail                        \
        </div>                          \
    </div>'
    for (let i = 0; i < mailings.length; i++) {
        table += compileRow(
            mailings[i]['id'], 
            mailings[i]['msg'], 
            mailings[i]['tag'], 
            mailings[i]['date_start'], 
            mailings[i]['date_end'], 
            mailings[i]['success'], 
            mailings[i]['fail']
        );
    }
    return table
}

function compileRow(id, msg, tag, date_start, date_end, success, fail) {
    let row = 
    `<div class="row">                      \
        <div class="cell cell_id">          \
            ${id}                           \
        </div>                              \
        <div class="cell cell_msg">         \
            ${msg}                          \
        </div>                              \
        <div class="cell cell_tag">         \
            ${tag}                          \
        </div>                              \
        <div class="cell cell_date">        \
            ${date_start}                   \
        </div>                              \
        <div class="cell cell_date">        \
            ${date_end}                     \
        </div>                              \
        <div class="cell cell_status">      \
            ${success}                      \
        </div>                              \
        <div class="cell cell_status">      \
            ${fail}                         \
        </div>                              \
    </div>`   
    return row        
}

function compileDetailTable(messages) {
    let table =
    '<div class="row first_row">                \
        <div class="cell cell_id">              \
            id                                  \
        </div>                                  \
        <div class="cell cell_client_id">       \
            client_id                           \
        </div>                                  \
        <div class="cell cell_date">            \
            date_create                         \
        </div>                                  \
        <div class="cell cell_status">          \
            status                              \
        </div>                                  \
    </div>'

    for (let i = 0; i < messages.length; i++) {
        table += compileDetailRow(
            messages[i]['id'], 
            messages[i]['client_id'], 
            messages[i]['date_create'], 
            messages[i]['status']
        );
    }
    return table
}


function compileDetailRow(id, client_id, date_create, status) {
    let row = 
    `<div class="row">                          \
        <div class="cell cell_id">              \
            ${id}                               \
        </div>                                  \
        <div class="cell cell_client_id">       \
            ${client_id}                        \
        </div>                                  \
        <div class="cell cell_date">            \
            ${date_create}                      \
        </div>                                  \
        <div class="cell cell_status">          \
            ${status}                           \
        </div>                                  \
     </div>`
     return row
}

async function sendRequestMenu1(type, inputs) {
    let url, json;
    if (type == 'create') {
        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i] == '') return;
        }
        url = 'create_mailing/';
        json = {
            'date_start': inputs[0],
            'date_end': inputs[1],
            'msg': inputs[2],
            'tag': inputs[3]
        }
    } else if (type == 'edit') {
        if (inputs[0] == '') return;
        url = 'update_mailing/';
        json = {
            'id': inputs[0],
        }
        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i] != '') {
                if (i == 1) {
                    json['date_start'] = inputs[1];
                } else if (i == 2) {
                    json['date_end'] = inputs[2];
                } else if (i == 3) {
                    json['msg'] = inputs[3];
                } else if (i == 4) {
                    json['tag'] = inputs[4];
                }
            }
            
        }
    } else {
        if (inputs[0] == '') return;
        url = 'delete_mailing/';
        json = {
            'id': inputs[0]
        }
    }
    let response = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(json),
        headers:{
			'Content-Type': 'application/json;charset=utf-8',
            'X-CSRFToken': document.cookie.split('csrftoken=')[1].split(';')[0]
        }
    });
    var json_response = await response.json();
    let status_request = 'Статус запроса: ';
    let menu1 = document.getElementById('menu-1');
    if (json_response['status'] == 'ok') {
        if (type == 'create') {
            status_request += `Рассылка созданна, ее id-${json_response['id']}`
        } else if (type == 'edit') {
            status_request += `Рассылка отредактированна`;
        } else {
            status_request += `Рассылка удалена`;
        }
        let input_objects = menu1.getElementsByTagName('input');
        for (let i = 0; i < input_objects.length; i++) {
            input_objects[i].value = '';
        }
    } else {
        if (json_response['desc'] == undefined) {
            status_request += 'Не корректный формат данных';
        } else {
            status_request += json_response['desc'];
        }
    }
    menu1.getElementsByClassName('status_request')[0].innerHTML = status_request;
}

async function sendRequestMenu2(type, inputs) {
    let url, json;
    if (type == 'create') {
        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i] == '') return;
        }
        url = 'create_client/';
        json = {
            'phone_number': inputs[0],
            'code': inputs[1],
            'tag': inputs[2],
            'time_zone': inputs[3]
        }
    } else if (type == 'edit') {
        if (inputs[0] == '') return;
        url = 'update_client/';
        json = {
            'id': inputs[0],
        }
        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i] != '') {
                if (i == 1) {
                    json['date_start'] = inputs[1];
                } else if (i == 2) {
                    json['date_end'] = inputs[2];
                } else if (i == 3) {
                    json['msg'] = inputs[3];
                } else if (i == 4) {
                    json['tag'] = inputs[4];
                }
            }
            
        }
    } else {
        if (inputs[0] == '') return;
        url = 'delete_client/';
        json = {
            'id': inputs[0]
        }
    }
    let response = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(json),
        headers:{
			'Content-Type': 'application/json;charset=utf-8',
            'X-CSRFToken': document.cookie.split('csrftoken=')[1].split(';')[0]
        }
    });
    var json_response = await response.json();
    let status_request = 'Статус запроса: ';
    let menu2 = document.getElementById('menu-2');
    if (json_response['status'] == 'ok') {
        if (type == 'create') {
            status_request += `Клиент созданн, его id-${json_response['id']}`
        } else if (type == 'edit') {
            status_request += `Клиент отредактированн`;
        } else {
            status_request += `Клиент удален`;
        }
        let input_objects = menu2.getElementsByTagName('input');
        for (let i = 0; i < input_objects.length; i++) {
            input_objects[i].value = '';
        }
    } else {
        if (json_response['desc'] == undefined) {
            status_request += 'Не корректный формат данных';
        } else {
            status_request += json_response['desc'];
        }
    }
    menu2.getElementsByClassName('status_request')[0].innerHTML = status_request;
}