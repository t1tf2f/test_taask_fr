from django.db import models


class Mailing(models.Model):
    id = models.AutoField(primary_key=True)
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()
    msg = models.CharField(max_length=255)
    tag = models.CharField(max_length=63)


class Client(models.Model):
    id = models.AutoField(primary_key=True)
    phone_number = models.CharField(max_length=11)
    code = models.CharField(max_length=5)
    tag = models.CharField(max_length=63)
    time_zone = models.IntegerField()


class Message(models.Model):
    id = models.AutoField(primary_key=True)
    date_create = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=15, default='created')
    client = models.ForeignKey('Client', on_delete=models.CASCADE)
    mailing = models.ForeignKey('Mailing', on_delete=models.CASCADE)

