from .serializers import *
from rest_framework.views import APIView

from rest_framework.response import Response
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema
from django.shortcuts import render
from .logger import Logger
from django import http
logger = Logger().getLogger("DEBUG")


def IndexPage(request):
    return http.HttpResponse(render(request, 'index.html'))


class CreateClient(APIView):
    @swagger_auto_schema(
        request_body=ClientCreateSerializer,
        operation_description='Создание нового клиента для рассылки',
        responses={
            '200': RespStatusOK_Id,
            '400': RespStatusFail
        }
    )
    def post(self, request):
        serializer = ClientCreateSerializer(data=request.data)
        if serializer.is_valid():
            client = serializer.save()
            resp = {
                'status': 'ok',
                'id': client.id
            }
            logger.info('Клиен успешно добавлен, id клиента = {}'.format(client.id))
            return Response(resp, status=status.HTTP_201_CREATED)
        logger.info('Ошибка сериализации данных при создании клиента')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UpdateClient(APIView):
    @swagger_auto_schema(
        request_body=ClientUpdateSerializer,
        operation_description='Редактирование клиента для рассылки',
        responses={
            '200': RespStatusOK_Id,
            '400': RespStatusFail
        }
    )
    def post(self, request):
        serializer = ClientUpdateSerializer(data=request.data)
        if serializer.is_valid():
            client = serializer.save()
            resp = {
                'status': 'ok',
                'id': client.id
            }
            logger.info('Информация о клиента обновлена, id клиента = {}'.format(client.id))
            return Response(resp, status=status.HTTP_200_OK)
        logger.info('Ошибка при сериализации данных для обновления клиента')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeleteClient(APIView):
    @swagger_auto_schema(
        request_body=ClientDeleteSerializer,
        operation_description='Удаление клиента для рассылки',
        responses={
            '200': RespStatusOK,
            '400': RespStatusFail
        }
    )
    def post(self, request):
        serializer = ClientDeleteSerializer(data=request.data)
        if serializer.is_valid():
            client = serializer.save()
            client_id = client.id
            client.delete()
            resp = {
                'status': 'ok'
            }
            logger.info('Клиент удален, id клиента = {}'.format(client_id))
            return Response(resp, status=status.HTTP_200_OK)
        logger.info('Ошибка при сериализции данных для удалении клиента')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CreateMailing(APIView):
    @swagger_auto_schema(
        request_body=MailingCreateSerializer,
        operation_description='Создание новогой рассылки',
        responses={
            '200': RespStatusOK_Id,
            '400': RespStatusFail
        }
    )
    def post(self, request):
        serializer = MailingCreateSerializer(data=request.data)
        if serializer.is_valid():
            mailing = serializer.save()
            resp = {
                'status': 'ok',
                'id': mailing.id
            }
            logger.info('Рассылка создана, id рассылки = {}'.format(mailing.id))
            return Response(resp, status=status.HTTP_200_OK)
        logger.info('Ошибка при сериализации данных для создания рассылки')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UpdateMailing(APIView):
    @swagger_auto_schema(
        request_body=MailingUpdateSerializer,
        operation_description='Редактирование рассылки',
        responses={
            '200': RespStatusOK_Id,
            '400': RespStatusFail
        }
    )
    def post(self, request):
        serializer = MailingUpdateSerializer(data=request.data)
        if serializer.is_valid():
            mailing = serializer.save()
            resp = {
                'status': 'ok',
                'id': mailing.id
            }
            logger.info('Рассылка обновлена, id рассылки = {}'.format(mailing.id))
            return Response(resp, status=status.HTTP_200_OK)
        logger.info('Ошибка при сериализации данных для обновления рассылки')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeletingMailing(APIView):
    @swagger_auto_schema(
        request_body=MailingDeleteSerializer,
        operation_description='Удаление рассылки',
        responses={
            '200': RespStatusOK,
            '400': RespStatusFail
        }
    )
    def post(self, request):
        serializer = MailingDeleteSerializer(data=request.data)
        if serializer.is_valid():
            mailing = serializer.save()
            mailing_id = mailing.id
            mailing.delete()
            resp = {
                'status': 'ok'
            }
            logger.info('Рассылка удалена, id рассылки = {}'.format(mailing_id))
            return Response(resp, status=status.HTTP_200_OK)
        logger.info('Ошибка при сериализации данных для удаления рассылки')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetGlobalStatistic(APIView):
    @swagger_auto_schema(
        operation_description='Получение статистики рассылок',
        responses={
            '200': RespGetGlobalStaticSerializer,
            '400': RespStatusFail
        })
    def get(self, request):
        resp = {
            'status': 'ok',
            'mailings': []
        }
        mailings = Mailing.objects.all()
        for mailing in mailings:
            messages = Message.objects.filter(mailing=mailing)
            success = 0
            fail = 0
            for message in messages:
                if message.status == 'success':
                    success += 1
                else:
                    fail += 1
            resp['mailings'].append({
                'id': mailing.id,
                'date_start': mailing.date_start,
                'date_end': mailing.date_end,
                'msg': mailing.msg,
                'tag': mailing.tag,
                'success': success,
                'fail': fail
            })
        logger.info('Выдача глобальной статистики, содержимое ответа = {}'.format(resp))
        return Response(resp, status=status.HTTP_200_OK)


class GetDetailStatistic(APIView):
    @swagger_auto_schema(
        request_body=GetDetailStatisticSerializer,
        operation_description='Получение детальной статистики по рассылке',
        responses={
            '200': RespStatusOKDetailStatistic,
            '400': RespStatusFail
        }
    )
    def post(self, request):
        serializer = GetDetailStatisticSerializer(data=request.data)
        if serializer.is_valid():
            mailing = serializer.save()
            messages = Message.objects.filter(mailing=mailing)
            resp = {
                'status': 'ok',
                'id': mailing.id,
                'messages': []
            }
            for msg in messages:
                resp['messages'].append({
                    "id": msg.id,
                    'date_create': msg.date_create,
                    'status': msg.status,
                    'client_id': msg.client.id
                })
            logger.info('Выдача детальной статистики, содержимое ответа = {}'.format(resp))
            return Response(resp, status=status.HTTP_200_OK)
        logger.info('Ошибка при сериализации данных для получения детальной статистики')
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



